package me.andriii25.andryplugin;

import org.bukkit.ChatColor;
import org.bukkit.Effect;
import org.bukkit.Location;
import org.bukkit.Sound;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerJoinEvent;

public class MyPlayerListener implements Listener{
	public static AndryPlugin plugin;
	
	@EventHandler
	public void onPlayerChat(AsyncPlayerChatEvent event) {
		Player player = event.getPlayer();
		
		if(event.getMessage().toLowerCase().contains("6heal")) {
			player.setHealth(20);
			player.setFireTicks(0);
			player.sendMessage(ChatColor.GREEN + "You have been healed!");
		}else if (event.getMessage().toLowerCase().contains("6kill")) {
			player.setHealth(0);
			player.sendMessage(ChatColor.DARK_RED + "" + ChatColor.BOLD + "YOU DIED!");
		}else if (event.getMessage().toLowerCase().contains("6ping")) {
			player.sendMessage("Pong!");
		}
	}
	@EventHandler
	public void onPlayerJoin(PlayerJoinEvent event) {
		Player player = event.getPlayer();
		Location pLoc = player.getLocation();
		World world = player.getWorld();
		world.playSound(pLoc, Sound.LEVEL_UP, 1, 0);
		event.setJoinMessage(
				ChatColor.GOLD + "Welcome to mah epac test server " + player.getName() + "." + "You are in: " 
				+ player.getWorld().getName() + ". It's " + player.getWorld().getTime() 
		);
	}
	@EventHandler
	public void onPlayerInteract(PlayerInteractEvent event) {
		Player player = event.getPlayer();
		Block block = player.getTargetBlock(null, 10);
		Location targetLoc = block.getLocation();
		World world = block.getWorld();
		int blockId = player.getItemInHand().getType().getId();
		if (blockId == 289) {
			world.playEffect(targetLoc, Effect.SMOKE, null);
		}else if (blockId == 331) {
			world.strikeLightning(targetLoc);
		}
	}
	

}
