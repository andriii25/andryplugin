package me.andriii25.andryplugin;

import java.util.Iterator;
import java.util.List;
import java.util.Arrays;
import java.util.Random;
import java.util.logging.Logger;

import org.bukkit.*;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.PluginDescriptionFile;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

public class AndryPlugin extends JavaPlugin {
	public final Logger logger = Logger.getLogger("Minecraft");
	public static AndryPlugin plugin;
	public final MyBlockListener bl = new MyBlockListener();
	public final MyPlayerListener pl = new MyPlayerListener();

	@Override
	public void onDisable() {
		PluginDescriptionFile pdfFile = this.getDescription();
		this.logger.info(pdfFile.getName() + " has been disabled!");

	}

	@Override
	public void onEnable() {
		PluginDescriptionFile pdfFile = this.getDescription();
		this.logger.info(pdfFile.getName() + " " + "Version" + " "
				+ pdfFile.getVersion() + " has been enabled!");
		PluginManager pm = getServer().getPluginManager();
		pm.registerEvents(this.bl, this);
		pm.registerEvents(this.pl, this);
		getConfig().options().copyDefaults();
		saveConfig();
		

	}

	public boolean onCommand(CommandSender sender, Command cmd,
			String commandLabel, String[] args) {

		Player player = (Player) sender;
		
		if (commandLabel.equalsIgnoreCase("sendme")) {
			if (args.length == 1) {
				player.sendMessage(ChatColor.UNDERLINE + args[0]);
			}
		}else if (commandLabel.equalsIgnoreCase("heal")
				|| commandLabel.equalsIgnoreCase("hea")) {
			if (args.length == 0) {
				player.setHealth(20);
				player.setFireTicks(0);
				player.sendMessage(ChatColor.GREEN + "You have been healed!");
			} else if (getServer().getPlayer(args[0]) != null) {
				Player targetPlayer = getServer().getPlayer(args[0]);
				targetPlayer.setHealth(20);
				targetPlayer.setFireTicks(0);
				targetPlayer.sendMessage(ChatColor.GREEN + "You have been healed by " + player.getDisplayName());
				player.sendMessage(ChatColor.GREEN + "You healed " + targetPlayer.getDisplayName());
			}
		}

		else if (commandLabel.equalsIgnoreCase("andryport")
				|| commandLabel.equalsIgnoreCase("anpo")) {
			if (args.length == 0) {
				player.sendMessage(ChatColor.BOLD + "Too few arguments!");
			} else if (args.length == 1) {
				Player targetPlayer = player.getServer().getPlayer(args[0]);
				Location targetPlayerLocation = targetPlayer.getLocation();
				player.teleport(targetPlayerLocation);
				if (!targetPlayer.isOnline()){
					player.sendMessage(ChatColor.RED + "" + targetPlayer.getDisplayName() + "" + " is not online!");
				}
				if (targetPlayer.isBanned()) {
					player.sendMessage(ChatColor.RED + "" + targetPlayer.getDisplayName() + "" + " is banned!");
				}

			} else if (args.length == 2) {
				Player targetPlayer = player.getServer().getPlayer(args[0]);
				Player targetPlayer1 = player.getServer().getPlayer(args[1]);
				Location targetPlayer1Location = targetPlayer1.getLocation();
				targetPlayer.teleport(targetPlayer1Location);
				if (!targetPlayer.isOnline()){
					player.sendMessage(ChatColor.RED + "" + targetPlayer.getDisplayName() + "" + " is not online!");
				}
				if (targetPlayer.isBanned()) {
					player.sendMessage(ChatColor.RED + "" + targetPlayer.getDisplayName() + "" + " is banned!");
				}
				if (!targetPlayer1.isOnline()){
					player.sendMessage(ChatColor.RED + "" + targetPlayer1.getDisplayName() + "" + " is not online!");
				}
				if (targetPlayer1.isBanned()) {
					player.sendMessage(ChatColor.RED + "" + targetPlayer1.getDisplayName() + "" + " is banned!");
				}
			} else if (args.length == 3) {
				double x = Double.parseDouble(args[0]);
				double y = Double.parseDouble(args[1]);
				double z = Double.parseDouble(args[2]);
				Location cords = new Location(player.getWorld(), x, y, z);
				player.teleport(cords);
			} else if (args.length == 4) {
				double x = Double.parseDouble(args[1]);
				double y = Double.parseDouble(args[2]);
				double z = Double.parseDouble(args[3]);
				Player targetPlayer = player.getServer().getPlayer(args[0]);
				Location cords = new Location(targetPlayer.getWorld(), x, y, z);
				targetPlayer.teleport(cords);

			} 
			else {
				player.sendMessage(ChatColor.DARK_RED + "Too much arguments");
			}
		}
		else if(commandLabel.equalsIgnoreCase("roll")){
			Random object = new Random();
			int i1;
			
			for (int i=1; i <= 1; i++) {
				i1 = object.nextInt(101);
				
				Bukkit.broadcastMessage(ChatColor.BOLD + "" + ChatColor.DARK_GRAY + player.getName() + " just rolled " + i1 + " from 100.");
				
				}
			}else if (commandLabel.equalsIgnoreCase("ko")){
				List<Player> onlinePlayers = Arrays.asList(Bukkit.getServer().getOnlinePlayers());
				Iterator<Player> iterator = onlinePlayers.iterator();
				while (iterator.hasNext()) {
					Player onlinePlayer = iterator.next();
					onlinePlayer.setHealth(0);
				}
				
			}else if (commandLabel.equalsIgnoreCase("motd")){
				player.sendMessage(ChatColor.GOLD + getConfig().getString("MOTD"));
			}
		return false;
	}
}
