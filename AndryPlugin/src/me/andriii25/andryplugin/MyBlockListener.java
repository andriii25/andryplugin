package me.andriii25.andryplugin;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.*;
import org.bukkit.event.block.BlockPlaceEvent;

public class MyBlockListener implements Listener{
	
	public static AndryPlugin plugin;
	public static Material[] blacklist = {Material.TNT, Material.BEDROCK, Material.LAVA};
	
	@EventHandler
	public void onBlockPlace(BlockPlaceEvent event) {
		Material block = event.getBlock().getType();
		Player player = event.getPlayer();
		
		
		for (Material blocked : blacklist) {
			if (blocked == block) {
				if (player.isOp()){}
					
				 else {
					event.getBlock().setType(Material.AIR);
					Bukkit.broadcastMessage(player.getDisplayName() + " just tried to place " + ChatColor.DARK_RED + block);
				}
			}
		}
	}
}
	

	
	
		


